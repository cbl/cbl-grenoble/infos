
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss

|FluxWeb| `RSS <https://cbl.frama.io/cbl-grenoble/infos/rss.xml>`_

.. _infos:

================================================================================
Infos **Cercle Bernard Lazare Grenoble** |CblGrenoble|
================================================================================

- https://www.cbl-grenoble.org/

.. toctree::
   :maxdepth: 6

   actions/actions



