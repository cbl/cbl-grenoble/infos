.. index::
   pair: CBL ; Israël-Palestine : Le combat pour la paix est aussi le nôtre (2004-01-29)
   ! Pacte de Genève

.. _cbl_2004_01_29:

================================================================================
2004-01-29 **Israël-Palestine : Le combat pour la paix est aussi le nôtre**
================================================================================

- https://www.cbl-grenoble.org/3-cbl-grenoble-7-action-5.html

Réunion-Débat Jeudi 29 janvier 2004 à 20h00
==================================================

à l'Auditorium du Musée, 5 place Lavalette, Grenoble, (tram Notre Dame-Musée)
Participation aux frais libre


A l'appel de

- Association France Palestine Solidarité - Grenoble
- Cercle Bernard Lazare - Grenoble (*)   (*) Membre fondateur du collectif `Deux peuples deux états <https://www.cbl-grenoble.org/3-cbl-grenoble-7-action-5.html#dpde>`_

Ouverture de la réunion par Michel Destot, Maire de Grenoble

Avec la participation de

- Samman Khoury ancien Directeur général du Ministère de l'Information,
  ancien Directeur du Centre de presse palestinien, actuellement coordinateur
  de la coalition des peuples pour la paix, il est une des figures clés des "Accords de Genève".
- Menahem Klein Professeur de Sciences Politiques à l'Université Bar Ilan, membre
  de "l'équipe de Geneve". Spécialiste de la question de Jérusalem, il a dirigé
  les discussions sur Jérusalem
- Daoud Barakat cadre de l'OLP, ancien Ambassadeur à Genève, Vienne et Moscou,
  ancien sous-directeur du département des réfugiés, et actuellement Commissaire
  adjoint pour Jérusalem.
- Mossi Raz Ancien député du Meretz à la Knesset, ancien Secrétaire général de
  Shalom Arshav (La paix maintenant), actuellement coordinateur de la coalition
  israélienne pour la paix et directeur de l'Institut de Guivat Haviva.


**Le pacte de Genève ou initiative de Genève est un projet d'accord fondé sur le principe « Deux peuples, deux Etats »**
===========================================================================================================================

Ce projet d'accord a pour caractéristique principale d'aborder toutes les
questions de front, avec précision et dans le détail, et c'est la nouveauté.

Le cadre de cet accord est celui du processus entamé à Madrid en 1991,
concrétisé par la déclaration des principes d'Oslo, les accords de Wye
River, Charm el Cheikh et poursuivi lors des négociations de Camp David
et Taba.  Un groupe d'application et de contrôle est prévu, comprenant
les Etats-Unis, l'Union Européenne, la Russie, les Nations Unies, auxquels
pourraient se joindre d'autres parties, régionales et internationales,
agréées par les signataires. Ce projet aborde les questions liées aux
territoires et frontières, aux colonies, au statut de l'Etat de Palestine,
au retrait d'Israël des territoires palestiniens. Ce projet fait également
des propositions concernant Jérusalem, la vieille ville de Jérusalem, le
devenir des réfugiés.


L'Association France Palestine Solidarité (AFPS)
----------------------------------------------------

L'Association France Palestine Solidarité (AFPS) est le résultat de la
fusion en 2001 de « France Palestine » et de « l'association médicale
franco-palestinienne ». Elle regroupe à Grenoble et dans l'Isère des personnes
attachées au droit des peuples à disposer d'eux-mêmes. L'association
apporte au peuple palestinien un soutien politique pour une paix réelle et
durable fondée sur l'application du droit international, en lien avec le
peuple palestinien et ses représentants légitimes. L'AFPS est aussi une
association humanitaire de solidarité morale et matérielle.

Le Cercle Bernard Lazare (CBL)
-------------------------------------

Le Cercle Bernard Lazare (CBL) est une association juive laïque de gauche,
militant depuis plus de 20 ans pour le dialogue israélo-palestinien.

Elle est indéfectiblement attachée à l'existence de l'Etat d'Israël en tant qu'Etat
du peuple juif, recours et référence de ce peuple pour la perennité de son
identité. Elle pense que la sécurité d'Israël ne pourra être assurée
militairement, mais seulement par une coexistence pacifique avec un Etat de
Palestine qu'il faut créer, ce qui implique la recherche active d'un compromis
par le dialogue et la négociation.

Nos associations ont en commun le fait de penser qu'il ne leur appartient
pas de décider quoi que ce soit à la place des peuples palestinien et
israélien. Mais, dans le même temps, nous affirmons que le combat pour la paix
est aussi le nôtre et c'est le sens de notre intiative commune.

Au-delà de nos différences, sans occulter nos divergences, dans le respect mutuel de
nos identités propres, notre appel commun à cette réunion-débat sur l'«
initiative de Genève » marque notre accord fondamental sur le fait que le
pacte de Genève apporte aujourd'hui l'espoir qu'une porte puisse s'ouvrir
vers la paix dans le dialogue entre Israéliens et Palestiniens. Le pacte de
Genève peut contribuer de manière décisive à tracer le chemin qui conduira
à la coexistence de deux Etats indépendants, libres et démocratiques. Ses
promoteurs le présentent seulement comme un modèle, et demandent à l'opinion
internationale de le diffuser et de le soutenir comme tel.
